package com.example.practicanum3.Model;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.practicanum3.R;

public class Activity_normal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal);
    }
}